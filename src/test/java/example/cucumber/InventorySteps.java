package example.cucumber;

import java.util.List;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class InventorySteps {

	@Given("the following inventory:")
	public void the_following_inventory(List<ProductInventoryInfo> inventory) {
	}

	@Then("inventory is:")
	public void inventoryIs(List<ProductInventoryInfo> inventory) {
	}

	@When("i order {int} items of product {string}")
	public void i_order_item_of_product(int amount, String productId) {
	}
}