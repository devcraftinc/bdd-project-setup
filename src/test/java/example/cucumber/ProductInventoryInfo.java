package example.cucumber;

public class ProductInventoryInfo {
	private String id;
	private int inventory;
	private int minInventory;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getInventory() {
		return inventory;
	}

	public void setInventory(int inventory) {
		this.inventory = inventory;
	}

	public int getMinInventory() {
		return minInventory;
	}

	public void setMinInventory(int minInventory) {
		this.minInventory = minInventory;
	}

}