package example.cucumber;

import java.util.List;

import org.junit.Assert;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class AdditionSteps {

	private int number;

	@Given("the number {int}")
	public void the_number(int number) {
		this.number = number;
	}

	@When("i add {int}")
	public void i_add(int increment) {
		number += increment;
	}

	@Then("the result is {int}")
	public void the_result_is(int expected) {
		Assert.assertEquals(expected, number);
	}

}