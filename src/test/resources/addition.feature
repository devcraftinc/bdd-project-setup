Feature: addition

  Scenario: add some numbers
    Given the number 1
    When i add 2
    Then the result is 3

  Scenario Outline: add some more numbers
    Given the number <number>
    When i add <increment>
    Then the result is <result>

    Examples: 
      | number | increment | result |
      |      1 |         2 |      3 |
      |      2 |         3 |      5 |
