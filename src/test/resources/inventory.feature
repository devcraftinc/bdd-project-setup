Feature: place an order

  Scenario: some scenario
    Given the following inventory:
      | product id | inventory | min inventory |
      | P1         |         3 |             1 |
      | P2         |         3 |             3 |
    When i order 1 items of product "P2"
    Then inventory is:
      | product id | inventory | min inventory |
      | P1         |         3 |             1 |
      | P2         |         3 |             2 |
