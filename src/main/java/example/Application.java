package example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application extends SpringApplication{

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Bean
	int example() {
		return 0;
	}
}
